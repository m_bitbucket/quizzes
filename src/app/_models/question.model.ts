export class Question {
  title: string;
  active: boolean = true;
  answers: string[] = [];
}
