import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class LogService {

  constructor(
    private http: Http
  ) { }

  private log_string(data, action) {
    const _string = {
      _id : data._id,
      time : new Date(),
      name : data.name,
      action : action
    };
    return _string;
  }

  getLogs() {
    return this.http.get(environment.host + environment.log).map(res => res.json());
  }

  add(data) {
    return this.http.post(environment.host + environment.log, this.log_string(data, 'add')).map(res => res.json());
  }

  delete_item(data) {
    return this.http.post(environment.host + environment.log, this.log_string(data, 'delete')).map(res => res.json());
  }

  edit(data) {
    return this.http.post(environment.host + environment.log, this.log_string(data, 'edit')).map(res => res.json());
  }

  pass(data) {
    return this.http.post(environment.host + environment.log, this.log_string(data, 'pass')).map(res => res.json());
  }
}
