import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BootstrapModule, AppRoutingModule } from './_modules/index';
import { QuizService, LogService} from './_services/index.services';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AddQuizComponent } from './quiz/add-quiz/add-quiz.component';
import { CardComponent, DeleteModelComponent } from './_directives/quiz-card/card.component';
import { QuestionComponent } from './_directives/question/question.component';
import { AddQuestionsComponent } from './_directives/add-questions/add-questions.component';
import { QuizComponent } from './quiz/quiz.component';
import { QuizResultComponent } from './quiz/quiz-result/quiz-result.component';
import { ResultsComponent } from './results/results.component';
import { LogsComponent } from './logs/logs.component';

// backend
import { MockBackend } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';
import { fakeBackendProvider } from './_mock/mock_backend.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AddQuizComponent,
    CardComponent,
    DeleteModelComponent,
    QuestionComponent,
    AddQuestionsComponent,
    QuizComponent,
    QuizResultComponent,
    ResultsComponent,
    LogsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BootstrapModule,

  ],
  providers: [
    QuizService,
    fakeBackendProvider,
    MockBackend,
    BaseRequestOptions,

    LogService
  ],
  entryComponents: [
    DeleteModelComponent,
    QuizComponent,
    QuizResultComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
