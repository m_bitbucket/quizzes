import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    ModalModule.forRoot(),
  ],
  exports: [
    ModalModule,
  ]
})

export class BootstrapModule {}
