import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { AddQuizComponent } from '../quiz/add-quiz/add-quiz.component';
import { ResultsComponent } from '../results/results.component';
import { LogsComponent } from '../logs/logs.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: HomeComponent },
  { path: 'new-quiz', component: AddQuizComponent },
  { path: 'edit/:id', component: AddQuizComponent },
  { path: 'results/:id', component: ResultsComponent },
  { path: 'logs', component: LogsComponent },
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
